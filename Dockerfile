FROM ubuntu:20.04
WORKDIR /usr/lib/env
RUN apt update
RUN ssh-keygen -t rsa -f ~/.ssh/id_rsa
RUN cat /root/.ssh/id_rsa.pub

